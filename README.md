# Neovid
This is my Neovim Configuration.
## Dependencies
· Neovim 5.5 or up (works perfect on 6.0)
· Nodejs 12.0 or up
· Nerdfont compatible font.

Neovid is a custom configuration for neovim that is one command install. Inspired by LunarVim.

Themes installed: Oceanic & Tokyonight

- ✨ Made for fun ✨

## 🔨 How to install
1. Install neovim package

MacOS:
```sh
brew install neovim
```
Ubuntu: (requires sudo & development branch added)
```sh
apt install neovim
```
Arch: (requires sudo)
```sh
pacman -Sy neovim
```
## 🎨 Recomended fonts
I recommend to install any glyp/icon supported font. You can find any here:
- https://www.nerdfonts.com/

The preconfigured font is FiraCode with Icon Support, you can download and install it from here:

<a href="https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip"><img src="https://raw.githubusercontent.com/ryanoasis/nerd-fonts/master/patched-fonts/FiraCode/extras/download.png" width="300" height="auto"></a>

## 📺 Additional information

I use coc.nvim as my LSP+function manager. Maybe in the future I'll change it to NeovimLSP.

##The COC pluggins preinstalled:

coc-actions
coc-css
coc-cssmodules
coc-emmet
coc-emoji
coc-git
coc-html
coc-json
coc-lists
coc-marketplace
coc-prettier
coc-pyright
coc-rust-analyzer
coc-sh
coc-smartf
coc-snippets
coc-svg
coc-tsserver
coc-vimlsp
coc-xml
coc-yaml
(All this extensions are part of the coc-nvim plugin and managed by neoclide: https://github.com/neoclide/coc.nvim)
