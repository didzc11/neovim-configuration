let g:tokyonight_style = "storm"
let g:tokyonight_italic_functions = 1
let g:tokyonight_sidebars = [ "qf", "vista_kind", "terminal", "packer" ]

let g:tokyonight_colors = {
      \ "hint" : "orange",
      \ "error" : "#9c2f2f",
      \ "none" : "NONE",
      \ "dark3" : "#545c7e",
      \ "comment" : "#565f89",
      \ "dark5" : "#737aa2",
      \ "blue0" : "#bb9af7",
      \ "blue" : "#F7768E",
      \ "cyan" : "#7aa2f7",
      \ "blue1" : "#9d7cd8",
      \ "blue2" : "#e0af68",
      \ "blue5" : "#0db9d7",
      \ "blue6" : "#73daca",
      \ "blue7" : "#41a6b5",
      \ "magenta" : "#bb9af7",
      \ "magenta2" : "#ff007c",
      \ "purple" : "#9d7cd8",
      \ "orange" : "#b9f27c",
      \ "yellow" : "#e0af68",
      \ "green" : "#73daca",
      \ "green1" : "#73daca",
      \ "green2" : "#41a6b5",
      \ "teal" : "#1abc9c",
      \ "red" : "#F7768E",
      \ "red1" : "#db4b4b"
      \}
colorscheme tokyonight
