" NVIM INIT FILE

if has('win32') || has ('win64')
    let $VIMHOME = $HOME."AppData\Local\nvim"
else
    let $VIMHOME = $HOME."/.config/nvim"
endif

" -- MODULES -- "
source $VIMHOME/modules/packages.vim
source $VIMHOME/modules/mapping.vim
source $VIMHOME/modules/explorer.vim
source $VIMHOME/modules/lualine.vim
source $VIMHOME/modules/colors.vim
source $VIMHOME/modules/bufferline.vim
source $VIMHOME/modules/dashboard.vim
source $VIMHOME/modules/autopairs.vim
" ------------- "

" -- COLORSCHEME -- "
" You can add your custom theme adding the file and sourcing it.
source $VIMHOME/themes/tokyonight.vim
" ----------------- "

" -- ESSENTIAL CONFIGURATION -- "
set number
set mouse=a
set clipboard=unnamedplus
set nocompatible
set cursorline
set hlsearch
set splitbelow
set splitright
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set autoindent
set termguicolors
set syntax=on
set encoding=UTF-8
set updatetime=300
set guifont=FiraCode\ Nerd\ Font:h11
" ----------------------------- "
" -- LSP -- "
source $VIMHOME/lsp/completion.vim
" --------- "
