lua << EOF
local get_mode = require('lualine.utils.mode').get_mode
local my_map = {
  ['NORMAL'] = ' Normal',
  ['INSERT'] = ' Insert',
  ['VISUAL'] = ' Visual',
  ['V-BLOCK'] = ' V-Block',
  ['V-LINE'] = '[] V-Visual',
  ['REPLACE'] = ' Replace',

}
local function my_mode()
  local mode = get_mode()
  if my_map[mode] == nil then return mode end
  return my_map[mode]
end

require('lualine').setup {
  options = {
    theme = 'tokyonight',
    component_separators = '|',
    section_separators = { left = '', right = '' },
  },
  sections = {
    lualine_a = {
      { my_mode, separator = { left = '' }, right_padding = 2 },
    },
    lualine_b = { 'filename', 'branch' },
    -- lualine_c = { 'fileformat', "require'lsp-status'.status()" }, old
    lualine_c = { 'fileformat', "coc#status" },
    lualine_x = {},
    lualine_y = { 'filetype', 'progress' },
    lualine_z = {
      { 'location', separator = { right = '' }, left_padding = 2 },
    },
  },
  inactive_sections = {
    lualine_a = { 'filename' },
    lualine_b = {},
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = { 'location' },
  },
  tabline = {},
  extensions = {'nvim-tree'},
}
EOF
