
if has('win32') || has ('win64')
    let $VIMMODULES = $HOME."AppData\Local\nvim\modules"
else
    let $VIMMODULES = $HOME."/.config/nvim/modules"
endif

source $VIMMODULES/colors.vim
source $VIMMODULES/lualine.vim
source $VIMMODULES/mapping.vim
source $VIMMODULES/explorer.vim
source $VIMMODULES/packages.vim
source $VIMMODULES/bufferline.vim
