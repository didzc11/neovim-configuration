
" -- EXECUTING CODE -- "
noremap <buffer> <F9> :exec '!python3' shellescape(@%, 1)<cr>
noremap <buffer> <F10> :TermExec cmd="cargo run"<cr>
" -------------------- "

" -- TERMINAL EXEC  -- "
autocmd TermEnter term://*toggleterm#*
      \ tnoremap <silent><F4> <Cmd>exe v:count1 . "ToggleTerm"<CR>
nnoremap <silent><F4> <Cmd>exe v:count1 . "ToggleTerm"<CR>
inoremap <silent><F4> <Esc><Cmd>exe v:count1 . "ToggleTerm"<CR>
" -------------------- "

" -- FILE EXPLORER  -- "
map <F12> :NvimTreeToggle<CR>
nnoremap <F11> :Telescope find_files<CR>
" -------------------- "

" -- MANAGE BUFFERS -- "
nmap <S-Tab> :bprevious<CR>
nmap <Tab> :bnext<CR>
map <C-x> :bw <cr>
" -------------------- "
