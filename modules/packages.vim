call plug#begin('~/.vim/plugged')
" -- INTERFACE -- "
Plug 'glepnir/dashboard-nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'akinsho/bufferline.nvim'
Plug 'nvim-lualine/lualine.nvim'
Plug 'ryanoasis/vim-devicons'
Plug 'norcalli/nvim-colorizer.lua'
" --------------- "

" -- VEHAVIOR  -- "
Plug 'windwp/nvim-autopairs'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'max397574/better-escape.nvim'
Plug 'scrooloose/nerdcommenter'
Plug 'liuchengxu/vim-which-key'
Plug 'akinsho/toggleterm.nvim'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'liuchengxu/vim-clap'
" --------------- "


" --    LSP    -- "
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'nvim-lua/lsp-status.nvim'
" --------------- "

" --  THEMES   -- "
Plug 'kien/rainbow_parentheses.vim'
Plug 'folke/tokyonight.nvim'
Plug 'rose-pine/neovim'
Plug 'catppuccin/nvim', {'as': 'catppuccin'}
" --------------- "

 
call plug#end()
