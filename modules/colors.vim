" -- ENABLED COLOR CONFIG -- "
" (disabled) let g:rbpt_max = 25
" (disabled) let g:rbpt_loadcmd_toggle = 0
" (disabled) au VimEnter * RainbowParenthesesToggle
" (disabled) au Syntax * RainbowParenthesesLoadRound
" (disabled) au Syntax * RainbowParenthesesLoadSquare
" (disabled) au Syntax * RainbowParenthesesLoadBraces
" (disabled) "au Syntax * RainbowparentesesLoadChevrons
" (disabled) "autocmd VimEnter * RainbowParenthesesLoadChevrons
" (disabled) autocmd VimEnter * RainbowParenthesesLoadBraces
" (disabled) autocmd VimEnter * RainbowParenthesesLoadSquare
" (disabled) autocmd VimEnter * RainbowParenthesesLoadRound
" -------------------------- "

"-- MORE THEMES FOR BRACKETS -- "

"Pastel colors palette
let g:rbpt_colorpairs = [
    \ ['brown',       '#f94144'],
    \ ['Darkblue',    '#f3722c'],
    \ ['darkgray',    '#f8961e'],
    \ ['darkgreen',   '#f9844a'],
    \ ['darkcyan',    '#f9c74f'],
    \ ['darkred',     '#90be6d'],
    \ ['darkmagenta', '#43aa8b'],
    \ ['brown',       '#4d908e'],
    \ ['gray',        '#577590'],
    \ ['black',       '#277da1'],
    \ ]

" -- COLOR DETECTION --"

lua << EOF

vim.opt.termguicolors = true
-- Attaches to every FileType mode
require 'colorizer'.setup()
EOF
" -------------------- "
