#!/bin/bash

installer() {
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
  local DISTRIB=$(awk -F= '/^NAME/{print $2}' /etc/os-release)

  if [[ ${DISTRIB} = "Ubuntu"* ]]; then
    if uname -a | grep -q '^Linux.*Microsoft'; then
      # ubuntu via WSL Windows Subsystem for Linux
      echo "WSL is currently not supported. :("
    else
      # native ubuntu
      echo "Ubuntu Detected!"
    fi

  elif [[ ${DISTRIB} = "Debian"* ]]; then
    echo "Debian Detected!"
    # debian
  elif [[ ${DISTRIB} = '"Arch Linux"' ]]; then
    echo "Arch Detected!"
    archinstall
  fi
elif [[ "$OSTYPE" == "darwin"* ]]; then
  echo "MacOS Detected!"
  # macOS OSX
fi
}
archinstall() {
  # Detect if neovim is installed, if it is, checks version and compatibility.
  is_nvim=$(whereis nvim | awk '{print $2}')
  if [[ $is_nvim == "/usr/bin/nvim" ]]; then
    nvim_version=$(nvim -v | head -1 | awk -F. '{print $2}')
    if [[ $nvim_version < 5 ]]; then
      echo "You have an incompatible version of neovim:"
      nvim -v | head -1
    else
      echo "You have a compatible version of neovim installed!"
      echo "Current version installed: $(nvim -v | head -1) "
    fi
  else
    echo "It seems like you don't have neovim installed."
    echo -n "Do you want to install it? (Y/n): "
    read -n 1 opt
    if [ $opt == "y" ] || [ $opt == "Y" ]; then
      echo "installing neovim:"
      sudo pacman -Sy neovim
    else
      echo "Exiting..."
      break
    fi
  fi

  # Detect if node is installed, if it is, checks version and compatibility.
  is_node=$(whereis node | awk '{print $2}')
  if [ $is_node == "/usr/bin/node" ]; then
    node_version=$(node -v | awk -F. '{ print $1 }' | awk -F "1" '{ print $2 }')
    if [ $node_version < 2 ]; then

    fi
  else
    echo "It seems like you don't have nodeJs installed."
    echo -n "Do you want to install it (Y/n): "
    read -n 1 opt
    if [ $opt == "y" ] || [ $opt == "Y" ]; then
      echo "installing nodeJs:"
      sudo pacman -S nodejs
    fi
  fi

  # Install nodejs extensions.
  echo "Do you want to install node extensions? (Y/n)"
  read -n 1 opt
  if [ $opt == "y" ] || [ $opt == "Y" ]; then
    echo "Installing nodejs extensions..."

  fi
  
  #Install VimPlug.
  echo installing VimPlug...
  pluginstall
  echo
}
pluginstall() {
  sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  echo Vimplug succesfully Installed!
}

PS3='[$]: '
options=("Install" "Uninstall" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Install")
          installer
            ;;
        "Uninstall")
            echo "you chose choice $REPLY which is $opt"
            ;;
        "Quit")
            break
            ;;
        *) echo "$REPLY is not a valid option.";;
    esac
done
